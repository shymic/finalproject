<%--
  Created by IntelliJ IDEA.
  User: Andrey
  Date: 25.03.2015
  Time: 10:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html><head><title>Error Page</title></head>
<body>
<%@ include file="/WEB-INF/jspf/init.jspf" %>

Request from ${pageContext.errorData.requestURI} is failed
<br/>
Servlet login or type: ${pageContext.errorData.servletName}
<br/>
Status code: ${pageContext.errorData.statusCode}
<br/>
Exception: ${pageContext.errorData.throwable}
<br/>
Exception: ${nullPage}
<br/><%@ include file="/WEB-INF/jspf/back.jspf" %>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body></html>
