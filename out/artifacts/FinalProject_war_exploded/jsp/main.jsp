<%--
  Created by IntelliJ IDEA.
  User: Andrey
  Date: 25.03.2015
  Time: 10:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/init.jspf" %>

<html><head><title><fmt:message key="main.welcome"/></title></head>
<body>

<h3><fmt:message key="main.welcome"/></h3>
<hr/>
<%@ include file="/WEB-INF/jspf/priseList.jspf" %>
<hr/>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body></html>