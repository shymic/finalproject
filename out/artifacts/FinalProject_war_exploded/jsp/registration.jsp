<%--
  Created by IntelliJ IDEA.
  User: Andrey
  Date: 12.04.2015
  Time: 0:32
  To change this template use File | Settings | File Templates.

--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jspf/init.jspf" %>

<html>
<head>
    <title> <fmt:message key="registration.title"/></title>
</head>
<body>

<h1><fmt:message key="registration.head"/></h1>
<form class="registration" name="registration" method="POST" action="controller">
    <input type="hidden" name="command" value="signup" />

    <input type="text" name="login" placeholder="<fmt:message key="registration.field.login"/>" value="" class="required" pattern="[\w]{4,16}"/>
    <p class="hints">*<fmt:message key="registration.login.hint"/></p>
    <br><p class="errors">${errorUserExistMessage}</p>
    <br/>
    <input type="password" name="password" placeholder="<fmt:message key="registration.field.password"/>" value="" class="required" pattern="[\w]{6,16}"/>
    <p class="hints">*<fmt:message key="registration.password.hint"/></p>

    <br/>
    <input type="password" name="confirm" placeholder="<fmt:message key="registration.field.confirm"/>" value="" class="required" pattern="[\w]{6,16}"/>
    <p class="hints">*</p>
    <p class="errors">${errorConfirmPassMessage}</p>

    <br/>
    <input type="text" name="budget" placeholder="<fmt:message key="registration.field.budget"/>" value="" class="required" pattern="[\d]*"/>
    <p class="hints">*</p>
    <br/>
    <input type="text" name="contact" placeholder="<fmt:message key="registration.field.contact"/>" value="" class="required" maxlength="255"/>
    <p class="hints">*<fmt:message key="registration.contact.hint"/></p>

    <br/>
    <p class="errors">${errorLoginPassMessage}</p>

    <p class="errors">${wrongAction}</p>

    <p class="errors">${nullPage}</p>

    <p class="errors">${emptyField}</p>
    <br/>
    <input type="submit" value="<fmt:message key="registration.button.submit"/>"/>
</form><hr/>
<%@ include file="/WEB-INF/jspf/static.jspf" %>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>
