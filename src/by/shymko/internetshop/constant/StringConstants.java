package by.shymko.internetshop.constant;

/**
 * Created by Andrey on 11.05.2015
 */
public class StringConstants {
    public static final String SESSION_ATTRRIBUTE_LOCALE = "locale";
    public static final String ERROR_NULL_PAGE = "nullPage";
    public static final String MESSAGE_NULLPAGE = "message.nullpage";
    public static final String PRISELIST_PARAMETER = "priseList";
    public static final String CLIENTLIST_PARAMETER = "clientList";
    public static final String ADMINLIST_PARAMETER = "adminList";
    public static final String PATH_PAGE_ERROR = "path.page.error";
    public static final String ADD_GOOD_PAGE = "path.page.addgood";
    public static final String PATH_PAGE_ADMIN = "path.page.admin";
    public static final String PATH_PAGE_MAIN = "path.page.main";
    public static final String PATH_PAGE_LOGIN = "path.page.login";
    public static final String PATH_PAGE_SHOW = "path.page.show";
    public static final String PATH_PAGE_REGISTRATION = "path.page.registration";
    public static final String CURRENT_USER_PARAMETER = "currentUser";

}
