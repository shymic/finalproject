package by.shymko.internetshop.dao;

import by.shymko.internetshop.exception.LogicalException;
import by.shymko.internetshop.good.Good;
import by.shymko.internetshop.list.ClientList;
import by.shymko.internetshop.list.GoodsList;
import by.shymko.internetshop.order.ShowOrderRecord;
import by.shymko.internetshop.user.Client;
import by.shymko.internetshop.user.User;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Andrey on 07.04.2015.
 */
public abstract class AbstractDAO<T extends User> {
    private static final Logger LOG = Logger.getLogger(AbstractDAO.class);
    public static final String COLUMN_USER_CONTACT = "contact";
    public static final String COLUMN_USER_ACTIVITY = "activity";
    public static final String TABLE_GOOD = "goodList";
    public static final String COLUMN_GOOD_ID = "id";
    public static final String COLUMN_GOOD_ID_ = "good.id";
    public static final String COLUMN_GOOD_NAME = "name";
    public static final String COLUMN_GOOD_SALE = "sale";
    public static final String COLUMN_GOOD_PRISE = "prise";
    public static final String COLUMN_GOOD_QUANTITY = "quantity";
    public static final String COLUMN_GOOD_INFO = "info";
    public static final String TABLE_USER = "clientList";
    public static final String SHOW_ORDER = "showOrder";
    public static final String COLUMN_USER_ID = "id";
    public static final String COLUMN_USER_LOGIN = "login";
    public static final String COLUMN_USER_PASSWORD = "password";
    public static final String COLUMN_USER_BUDGET = "budget";
    public static final String COLUMN_USER_ROLE = "role";
    public static final String COLUMN_ORDER_QUANTITY = "number";
    public static final String COLUMN_GOOD_NAME_ = "good.name";
    public static final String COLUMN_CLIENT_NAME_= "user.login";
    public static final String COLUMN_ORDER_QUANTITY_ = "client_order.number";

    protected Connection connection;

    public AbstractDAO(Connection connection) {
        this.connection = connection;
    }

    /**
     * @param idUser
     * @return
     * @throws LogicalException
     */
    public abstract T selectUserByID(int idUser) throws LogicalException;

    /**
     * @param loginUser
     * @return
     * @throws LogicalException
     */
    public abstract T selectUserByLogin(String loginUser) throws LogicalException;

    /**
     * @param login
     * @param password
     * @return
     * @throws LogicalException
     */
    public abstract T selectUserByLoginAndPassword(String login, String password) throws LogicalException;

    /**
     * @return
     * @throws LogicalException
     */
    public GoodsList selectGoodList() throws LogicalException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(DAOManager.getProperty(TABLE_GOOD));
            ps.execute();
            ResultSet rs = ps.getResultSet();
            GoodsList priseList = new GoodsList();
            while (rs.next()){
                priseList.add(new Good(rs.getInt(COLUMN_GOOD_ID), rs.getString(COLUMN_GOOD_NAME),rs.getInt(COLUMN_GOOD_PRISE),
                        rs.getInt(COLUMN_GOOD_SALE), rs.getString(COLUMN_GOOD_INFO), rs.getInt(COLUMN_GOOD_QUANTITY)));
            }
            return  priseList;
        } catch (SQLException e) {
            throw new LogicalException(e);
        }

    }

    /**
     * @return
     */
    public ClientList selectClientList() {
        ClientList clients = new ClientList();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(DAOManager.getProperty(TABLE_USER));
            ps.execute();
            ResultSet rs = ps.getResultSet();
            while (rs.next()){
                clients.add(new Client(rs.getInt(COLUMN_USER_ID), rs.getString(COLUMN_USER_LOGIN), rs.getString(COLUMN_USER_PASSWORD),
                        rs.getString(COLUMN_USER_CONTACT), rs.getInt(COLUMN_USER_BUDGET), rs.getBoolean(COLUMN_USER_ACTIVITY)));
            }
        } catch (SQLException e) {
            LOG.info(e);
        }finally {
            close(ps);
        }
        return  clients;
    }

    /**
     * @param idClient
     * @return
     */
    public ArrayList<ShowOrderRecord> selectOrderbByClientId(int idClient){
        ArrayList<ShowOrderRecord> order = new ArrayList<>();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(DAOManager.getProperty(SHOW_ORDER));
            ps.setInt(1, idClient);
            ps.execute();
            ResultSet rs = ps.getResultSet();
            while (rs.next()) {
                order.add(new ShowOrderRecord(rs.getInt(COLUMN_GOOD_ID_),rs.getString(COLUMN_GOOD_NAME_), rs.getString(COLUMN_CLIENT_NAME_), rs.getInt(COLUMN_ORDER_QUANTITY_)));
            }

            } catch (SQLException e) {
            LOG.info(e);
        }finally {
            close(ps);
        }
        return  order;
    }

    /**
     * @param client
     * @return
     */
    public abstract boolean addClient(Client client);

    /**
     * @param good
     * @return
     */
    public abstract boolean addGood(Good good);

    /**
     * @param order
     * @param user
     * @return
     */
    public abstract boolean registerOrder(Map<Integer, Integer> order, User user);

    /**
     * @param st
     */
    public void close(Statement st) {
        try {
            if (st != null) {
                st.close();
            }
        } catch (SQLException e) {
            LOG.info("Can't close statement" + e);
        }
    }

}
