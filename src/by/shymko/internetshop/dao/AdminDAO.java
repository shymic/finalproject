package by.shymko.internetshop.dao;

import by.shymko.internetshop.exception.LogicalException;
import by.shymko.internetshop.good.Good;
import by.shymko.internetshop.user.Administrator;
import by.shymko.internetshop.user.Client;
import by.shymko.internetshop.user.User;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Andrey on 10.05.2015
 */
public class AdminDAO extends AbstractDAO {
    private static final String INSERT_GOOD = "insertNewGood";
    private static final String TABLE_ADMIN = "selectAdminList";
    private static final String DELETE_CLIENT = "deleteClient";
    private static final String DELETE_GOOD = "deleteGood";
    private static final String DELETE_GOOD_FROM_ORDER = "deleteGoodFromOrder";
    private static final String DELETE_CLIENT_FROM_ORDER = "deleteClientFromOrderList";
    private static final Logger LOG = Logger.getLogger(AdminDAO.class);

    public AdminDAO(Connection connection) {
        super(connection);
    }

    @Override
    public User selectUserByID(int idUser) throws LogicalException {
        return null;
    }

    @Override
    public User selectUserByLogin(String loginUser) throws LogicalException {
        return null;
    }

    @Override
    public User selectUserByLoginAndPassword(String login, String password) throws LogicalException {
        return null;
    }

    @Override
    public boolean addClient(Client client) {
        return false;
    }

    /**
     * @param good
     * @return
     */
    @Override
    public boolean addGood(Good good) {
        PreparedStatement ps = null;
        boolean result = false;
        try {
            ps = connection.prepareStatement(DAOManager.getProperty(INSERT_GOOD));
            ps.setInt(1, good.getId());
            ps.setString(2, good.getName());
            ps.setInt(3, good.getPrise());
            ps.setInt(4, good.getSale());
            ps.setString(5, good.getInfo());
            ps.setInt(6, good.getQuantity());
            ps.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOG.info(e);
        } finally {
            close(ps);
        }
        return result;
    }

    @Override
    public boolean registerOrder(Map order, User user) {
        return false;
    }

    /**
     * @param id
     * @return
     */
    public boolean deleteClientById(int id){
        PreparedStatement ps = null;
        boolean result = false;
        try {
            ps = connection.prepareStatement(DAOManager.getProperty(DELETE_CLIENT_FROM_ORDER));
            ps.setInt(1, id);
            ps.executeUpdate();
            ps = connection.prepareStatement(DAOManager.getProperty(DELETE_CLIENT));
            ps.setInt(1, id);
            ps.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOG.info(e);
        } finally {
            close(ps);
        }
        return result;
    }

    /**
     * @param id
     * @return
     */
    public boolean deleteGoodById(int id){
        PreparedStatement ps = null;
        boolean result = false;
        try {
            ps = connection.prepareStatement(DAOManager.getProperty(DELETE_GOOD));
            ps.setInt(1, id);
            ps.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOG.info(e);
        } finally {
            close(ps);
        }
        return result;
    }

    /**
     * @param id
     * @return
     */
    public boolean deleteGoodByIdFromOrder(int id){
        PreparedStatement ps = null;
        boolean result = false;
        try {
            ps = connection.prepareStatement(DAOManager.getProperty(DELETE_GOOD_FROM_ORDER));
            ps.setInt(1, id);
            ps.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOG.info(e);
        } finally {
            close(ps);
        }
        return result;
    }

    /**
     * @return
     */
    public ArrayList<Administrator> selectAdminList() {
        ArrayList<Administrator> administrators = new ArrayList<>();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(DAOManager.getProperty(TABLE_ADMIN));
            ps.execute();
            ResultSet rs = ps.getResultSet();
            while (rs.next()){
                administrators.add(new Administrator(rs.getInt(COLUMN_USER_ID), rs.getString(COLUMN_USER_LOGIN), rs.getString(COLUMN_USER_PASSWORD),
                        rs.getString(COLUMN_USER_CONTACT)));
            }
        } catch (SQLException e) {
            LOG.info(e);
        }finally {
            close(ps);
        }
        return  administrators;
    }
}
