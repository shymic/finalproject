package by.shymko.internetshop.dao;

import java.util.ResourceBundle;

/**
 * Created by Andrey on 13.04.2015.
 */
public class DAOManager {

    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources/daoconfig");

    private DAOManager() {
    }

    /**
     * @param key
     * @return
     */
    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}