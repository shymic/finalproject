package by.shymko.internetshop.dao;

import by.shymko.internetshop.exception.LogicalException;
import by.shymko.internetshop.good.Good;
import by.shymko.internetshop.order.OrderRecord;
import by.shymko.internetshop.user.Administrator;
import by.shymko.internetshop.user.Client;
import by.shymko.internetshop.user.User;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Set;

/**
 * Created by Andrey on 13.04.2015.
 */
public class UserDAO extends AbstractDAO {
    private static final Logger LOG = Logger.getLogger(UserDAO.class);
    private static final String FIND_USER_BY_LOGIN = "userByLogin";
    private static final String FIND_USER_BY_ID = "userByID";
    private static final String SIGN_IN = "signInInfo";
    private static final String INSERT_CLIENT = "insertClientInUser";
    private static final String REGISTER_ORDER = "registerOrder";
    private static final String ADD_EXIST_ORDER = "addExistOrder";
    private static final String SELECT_ORDER_RECORD = "selectOrderRecord";


    public UserDAO(Connection connection) {
        super(connection);
    }

    /**
     * @param idUser
     * @return
     * @throws LogicalException
     */
    @Override
    public User selectUserByID(int idUser) throws LogicalException {
        PreparedStatement ps = null;
        User user = null;
        try {
            ps = connection.prepareStatement(DAOManager.getProperty(FIND_USER_BY_ID));
            ps.setInt(1, idUser);
            ps.execute();
            ResultSet rs = ps.getResultSet();
            if (rs.next()) {
                int readId = rs.getInt(COLUMN_USER_ID);
                String readLogin = rs.getString(COLUMN_USER_LOGIN);
                String readPassword = rs.getString(COLUMN_USER_PASSWORD);
                String readContact = rs.getString(COLUMN_USER_CONTACT);
                switch (rs.getString(COLUMN_USER_ROLE)) {
                    case "ADMIN": {
                        user = new Administrator(readId, readLogin, readLogin, readContact);
                        break;
                    }
                    case "CLIENT": {
                        Client client = new Client(readId, readLogin, readPassword, readContact);
                        client.setBudget(rs.getInt(COLUMN_USER_BUDGET));
                        client.setActivity(rs.getBoolean(COLUMN_USER_ACTIVITY));
                        user = client;
                        break;
                    }
                    default:{
                        throw new LogicalException("invalid note in database");
                    }
                }
            }
        } catch (SQLException e) {
            LOG.info(e);
        } finally {
            close(ps);
        }
        return user;
    }

    /**
     * @param loginUser
     * @return
     * @throws LogicalException
     */
    @Override
    public User selectUserByLogin(String loginUser) throws LogicalException {
        PreparedStatement ps = null;
        User user = null;
        try {
            ps = connection.prepareStatement(DAOManager.getProperty(FIND_USER_BY_LOGIN));
            ps.setString(1, loginUser);
            ps.execute();
            ResultSet rs = ps.getResultSet();
            if (rs.next()) {
                int readId = rs.getInt(COLUMN_USER_ID);
                String readLogin = rs.getString(COLUMN_USER_LOGIN);
                String readPassword = rs.getString(COLUMN_USER_PASSWORD);
                String readContact = rs.getString(COLUMN_USER_CONTACT);
                switch (rs.getString(COLUMN_USER_ROLE)) {
                    case "ADMIN": {
                        user = new Administrator(readId, readLogin, readLogin, readContact);
                        break;
                    }
                    case "CLIENT": {
                        Client client = new Client(readId, readLogin, readPassword, readContact);
                        client.setBudget(rs.getInt(COLUMN_USER_BUDGET));
                        client.setActivity(rs.getBoolean(COLUMN_USER_ACTIVITY));
                        user = client;
                        break;
                    }
                    default:{
                        throw new LogicalException("invalid note in database");
                    }
                }
            }
        } catch (SQLException e) {
            LOG.info(e);
        } finally {
            close(ps);
        }
        return user;
    }

    /**
     * @param login
     * @param password
     * @return
     * @throws LogicalException
     */
    @Override
    public User selectUserByLoginAndPassword(String login, String password) throws LogicalException {
        PreparedStatement ps = null;
        User user = null;
        try {
            ps = connection.prepareStatement(DAOManager.getProperty(SIGN_IN));
            ps.setString(1, login);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int readId = rs.getInt(COLUMN_USER_ID);
                String readLogin = rs.getString(COLUMN_USER_LOGIN);
                String readPassword = rs.getString(COLUMN_USER_PASSWORD);
                String readContact = rs.getString(COLUMN_USER_CONTACT);
                switch (rs.getString(COLUMN_USER_ROLE)) {
                    case "ADMIN": {
                        user = new Administrator(readId, readLogin, readLogin, readContact);break;
                    }
                    case "CLIENT": {
                        Client client = new Client(readId, readLogin, readPassword, readContact);
                        client.setBudget(rs.getInt(COLUMN_USER_BUDGET));
                        client.setActivity(rs.getBoolean(COLUMN_USER_ACTIVITY));
                        user = client;
                        break;
                    }
                    default:{
                        throw new LogicalException("invalid note in database");
                    }
                }
            }
        } catch (SQLException e) {
            LOG.info(e);
        } finally {
            close(ps);
        }
        return user;
    }

    /**
     * @param clientId
     * @param goodId
     * @return
     */
    public OrderRecord checkContainsInDB(int clientId, int goodId) {
        OrderRecord orderRecord = null;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(DAOManager.getProperty(SELECT_ORDER_RECORD));
            ps.setInt(1, clientId);
            ps.setInt(2, goodId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int readQuantity = rs.getInt(COLUMN_ORDER_QUANTITY);
                orderRecord = new OrderRecord(clientId, goodId, readQuantity);
            }

        } catch (SQLException e) {
            LOG.info(e);
        } finally {
            close(ps);
        }
        return orderRecord;
    }

    /**
     * @param order
     * @param user
     * @return
     */
    @Override
    public boolean registerOrder(Map order, User user) {
        PreparedStatement ps = null;
        PreparedStatement psExist = null;
        boolean flag = false;
        Set<Integer> keys = order.keySet();
        try {
            ps = connection.prepareStatement(DAOManager.getProperty(REGISTER_ORDER));
            psExist = connection.prepareStatement(DAOManager.getProperty(ADD_EXIST_ORDER));

            for (Integer key : keys) {
                OrderRecord record = checkContainsInDB(user.getId(), key);
                if (record == null) {

                    ps.setInt(1, user.getId());
                    ps.setInt(2, key);
                    ps.setInt(3, (Integer) order.get(key));
                    ps.executeUpdate();
                }else{

                    psExist.setInt(1, record.getQuantity()+(Integer) order.get(key));
                    psExist.setInt(2, user.getId());
                    psExist.setInt(3, key);
                    psExist.execute();
                }
            }
            flag = true;
        } catch (SQLException e) {
            LOG.info(e);
        } finally {
            close(ps);
            close(psExist);
        }
        return flag;
    }

    /**
     * @param client
     * @return
     */
    @Override
    public boolean addClient(Client client) {
        PreparedStatement ps = null;
        boolean result = false;
        try {
            ps = connection.prepareStatement(DAOManager.getProperty(INSERT_CLIENT));
            ps.setInt(1, client.getId());
            ps.setString(2, client.getLogin());
            ps.setString(3, client.getPassword());
            ps.setString(4, client.getContact());
            ps.setString(5, client.getRole().name());
            ps.setInt(6, client.getBudget());
            ps.setBoolean(7, client.isActivity());
            ps.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOG.info(e);
        } finally {
            close(ps);
        }
        return result;
    }

    @Override
    public boolean addGood(Good good) {
        return false;
    }
}
