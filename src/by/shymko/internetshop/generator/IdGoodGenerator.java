package by.shymko.internetshop.generator;

import by.shymko.internetshop.dao.DAOManager;
import by.shymko.internetshop.pool.PoolConnection;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Andrey on 09.05.2015
 */
public class IdGoodGenerator {
    private static final Logger LOG = Logger.getLogger(IdClientGenerator.class);
    public static final String SELECT_MAX_GOOD_ID = "select.maxGoodId";
    private static int generatedId =selectMaxId();

    /**
     * @return
     */
    private static int selectMaxId() {
        Connection connection = PoolConnection.getPool().getConnection();
        PreparedStatement ps = null;
        int i = 0;
        try {
            ps = connection.prepareStatement(DAOManager.getProperty(SELECT_MAX_GOOD_ID));
            ps.execute();
            ResultSet rs = ps.getResultSet();
            if (rs.next()) {
                i = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOG.info(e);
        } finally {
            PoolConnection.returnConnection(connection);
        }
        return i;
    }

    /**
     * @return
     */
    public static int generate() {
        return ++generatedId;
    }
}

