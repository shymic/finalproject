package by.shymko.internetshop.good;

import by.shymko.internetshop.exception.LogicalException;
import org.apache.log4j.Logger;

/**
 * Created by Andrey on 17.02.2015.
 */
public class Good {
    private static final Logger LOG = Logger.getLogger(Good.class);

    private int id;
    private String name;
    private int prise;
    private int sale;
    private String info;
    private int quantity;

    public Good() {
    }

    public Good(int id, String name, int prise, int sale, String info, int quantity) {
        this.id = id;
        this.name = name;
        this.prise = prise;
        this.sale = sale;
        this.info = info;
        this.quantity = quantity;
    }


    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) throws LogicalException {
        if (quantity < 0) {
            LOG.info("invalid quantity");
            throw new LogicalException();
        }
        this.quantity = quantity;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) throws LogicalException {
        if ( info.length()==0){
            throw new LogicalException("empty info");
        }
        this.info = info;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) throws LogicalException {
        if (id < 0) {
            LOG.info("invalid id");
            throw new LogicalException();
        }
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws LogicalException {
        if (name.length() == 0) {
            LOG.info("invalid good's name");
            throw new LogicalException();
        }
        this.name = name;
    }

    public int getPrise() {
        return prise;
    }

    public void setPrise(int prise) throws LogicalException {
        if (prise <= 0) {
            LOG.info("invalid prise");
            throw new LogicalException();
        }
        this.prise = prise;
    }

    public int getSale() {
        return sale;
    }

    public void setSale(int sale) throws LogicalException {
        if (sale <= 0 || sale >= this.prise) {
            LOG.info("invalid sale");
            throw new LogicalException();
        }
        this.sale = sale;
    }

    @Override
    public String toString() {
        return "Good{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", prise=" + prise +
                ", sale=" + sale +
                ", info='" + info + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
