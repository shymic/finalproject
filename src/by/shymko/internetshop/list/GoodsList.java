package by.shymko.internetshop.list;

import by.shymko.internetshop.good.Good;

import java.util.ArrayList;

/**
 * Created by Andrey on 17.02.2015.
 */
public class GoodsList extends ArrayList<Good> {
    public GoodsList(int initialCapacity) {
        super(initialCapacity);
    }

    public GoodsList() {
    }

    /**
     * @param id
     * @return
     */
    public boolean removeById(int id) {
        for (int i = 0; i < this.size(); ++i) {
            if (this.get(i).getId() == id) {
                this.remove(i);
                return true;
            }
        }
        return false;
    }

    /**
     * @param id
     * @return
     */
    public Good getById(int id) {
        Good tmp = null;
        for (int i = 0; i < this.size(); ++i) {
            if (this.get(i).getId() == id) {
                tmp = this.get(i);
            }
        }
        return tmp;
    }

}
