package by.shymko.internetshop.order;

/**
 * Created by Andrey on 08.05.2015.
 */
public class OrderRecord {
    private int clientId;
    private int goodId;
    private int quantity;

    public OrderRecord() {
    }

    public OrderRecord(int clientId, int goodId, int quantity) {
        this.clientId = clientId;
        this.goodId = goodId;
        this.quantity = quantity;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getGoodId() {
        return goodId;
    }

    public void setGoodId(int goodId) {
        this.goodId = goodId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
