package by.shymko.internetshop.order;

/**
 * Created by Andrey on 10.05.2015
 */
public class ShowOrderRecord {
    private int idGood;
    private String orderGoodName;
    private String orderClientName;
    private int orderQuantity;

    public ShowOrderRecord() {
    }

    public ShowOrderRecord(String orderGoodName, String orderClientName, int orderQuantity) {
        this.orderGoodName = orderGoodName;
        this.orderClientName = orderClientName;
        this.orderQuantity = orderQuantity;
    }

    public ShowOrderRecord(int idGood, String orderGoodName, String orderClientName, int orderQuantity) {
        this.idGood = idGood;
        this.orderGoodName = orderGoodName;
        this.orderClientName = orderClientName;
        this.orderQuantity = orderQuantity;
    }

    public int getIdGood() {
        return idGood;
    }

    public void setIdGood(int idGood) {
        this.idGood = idGood;
    }

    public String getOrderGoodName() {
        return orderGoodName;
    }

    public void setOrderGoodName(String orderGoodName) {
        this.orderGoodName = orderGoodName;
    }

    public String getOrderClientName() {
        return orderClientName;
    }

    public void setOrderClientName(String orderClientName) {
        this.orderClientName = orderClientName;
    }

    public int getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(int orderQuantity) {
        this.orderQuantity = orderQuantity;
    }
}
