package by.shymko.internetshop.pool;

import by.shymko.internetshop.exception.LogicalException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Andrey on 05.04.2015.
 */
public class PoolConnection{
    public static  final String RESOURCE_DB = "resources/database";
    public static  final String CONNECTION_DB_URL_PARAMETER = "url";
    public static  final String CONNECTION_DB_USER_PARAMETER = "user";
    public static  final String CONNECTION_DB_PASSWORD_PARAMETER = "pass";
    public static  final String CONNECTION_DB_USER = "user";
    public static  final String CONNECTION_DB_PASSWORD = "password";
    private Lock lock = new ReentrantLock();
    private ConcurrentLinkedQueue<Connection> connections;
    public final static int SIZE = 5;

    private PoolConnection() {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            connections = new ConcurrentLinkedQueue<>();
            ResourceBundle resource = ResourceBundle.getBundle(RESOURCE_DB);
            String url = resource.getString(CONNECTION_DB_URL_PARAMETER);
            String user = resource.getString(CONNECTION_DB_USER_PARAMETER);
            String pass = resource.getString(CONNECTION_DB_PASSWORD_PARAMETER);
            Properties prop = new Properties();
            prop.put(CONNECTION_DB_USER, user);
            prop.put(CONNECTION_DB_PASSWORD, pass);
            for (int i = 0; i < SIZE; i++) {
                connections.add(DriverManager.getConnection(url, prop));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Can't connect to DB"  + e);
        }
    }

    private static class PoolHolder {
        public static final PoolConnection pool = new PoolConnection();
    }

    public static PoolConnection getPool() {
        return PoolHolder.pool;
    }

    /**
     * @return
     */
    private ConcurrentLinkedQueue<Connection> getConnections() {
        return connections;
    }

    public Connection getConnection(){
        lock.lock();
        try{
            Connection res = getPool().getConnections().poll();
            return res;
        }finally {
            lock.unlock();
        }
    }

    /**
     * @param res
     */
    public static void returnConnection(Connection res) {
        getPool().getConnections().add(res);
    }

    /**
     * @throws LogicalException
     */
    public static void releasePool() throws LogicalException {
        while (!getPool().getConnections().isEmpty()) {
            try {
                getPool().getConnections().poll().close();
            } catch (SQLException e) {
                throw new LogicalException("can't close connection");
            }
        }
    }

}
