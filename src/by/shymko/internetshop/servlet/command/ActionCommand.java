package by.shymko.internetshop.servlet.command;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Andrey on 25.03.2015
 */
public interface ActionCommand {
    String execute(HttpServletRequest request);
}