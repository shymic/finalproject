package by.shymko.internetshop.servlet.command;

import by.shymko.internetshop.servlet.manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import static  by.shymko.internetshop.constant.StringConstants.ADD_GOOD_PAGE;

/**
 * Created by Andrey on 09.05.2015
 */
public class AddCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
        return  ConfigurationManager.getProperty(ADD_GOOD_PAGE);
    }
}
