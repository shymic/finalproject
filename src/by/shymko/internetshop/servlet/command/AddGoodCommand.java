package by.shymko.internetshop.servlet.command;

import by.shymko.internetshop.exception.LogicalException;
import by.shymko.internetshop.generator.IdGoodGenerator;
import by.shymko.internetshop.good.Good;
import by.shymko.internetshop.servlet.logic.AddGoodLogic;
import by.shymko.internetshop.servlet.manager.ConfigurationManager;
import by.shymko.internetshop.servlet.manager.MessageManager;
import by.shymko.internetshop.validator.Validator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static by.shymko.internetshop.constant.StringConstants.SESSION_ATTRRIBUTE_LOCALE;

/**
 * Created by Andrey on 09.05.2015
 */
public class AddGoodCommand implements ActionCommand {
    private static final Logger LOG = Logger.getLogger(AddGoodCommand.class);
    private static final String REQUEST_PARAMETER_GOOD_NAME = "name";
    private static final String REQUEST_PARAMETER_GOOD_SALE = "sale";
    private static final String REQUEST_PARAMETER_GOOD_PRISE = "prise";
    private static final String REQUEST_PARAMETER_GOOD_INFO = "info";
    private static final String REQUEST_PARAMETER_GOOD_QUANTITY = "quantity";
    private static final String ERROR_ADD_GOOD_PARAMETER = "errorAddGoodMessage";
    private static final String COMPLETE_ADD_GOOD_PARAMETER = "completeAddGoodMessage";
    private static final String ERROR_EMPTY_FIELD_PARAMETER = "emptyField";
    private static final String MESSAGE_ERROR_ADD_GOOD = "message.error.addgood";
    private static final String MESSAGE_COMPLETE_ADD_GOOD = "message.complete.addgood";
    private static final String PATH_PAGE_ADD_GOOD = "path.page.addgood";
    private static final String MESSAGE_EMPTY_FIELD = "message.emptyfield";


    @Override
    public String execute(HttpServletRequest request) {
        MessageManager messageManager = new MessageManager((String) request.getSession().getAttribute(SESSION_ATTRRIBUTE_LOCALE));
        Good newGood = new Good();
        String page = null;
        try {
            newGood.setId(IdGoodGenerator.generate());
            newGood.setName(request.getParameter(REQUEST_PARAMETER_GOOD_NAME));
            String readPrise = request.getParameter(REQUEST_PARAMETER_GOOD_PRISE);
            String readSale = request.getParameter(REQUEST_PARAMETER_GOOD_SALE);
            String readQuantity = request.getParameter(REQUEST_PARAMETER_GOOD_QUANTITY);
            if( readPrise.length() == 0 || readSale.length() == 0 || readQuantity.length()== 0){
                throw new LogicalException("empty fields");
            }
            newGood.setPrise(Integer.parseInt(readPrise));
            newGood.setSale(Integer.parseInt(readSale));
            newGood.setQuantity(Integer.parseInt(readQuantity));
            newGood.setInfo(request.getParameter(REQUEST_PARAMETER_GOOD_INFO));
            if(Validator.validateGood(newGood)) {
                if (AddGoodLogic.add(newGood)) {
                    request.setAttribute(COMPLETE_ADD_GOOD_PARAMETER,
                            messageManager.getProperty(MESSAGE_COMPLETE_ADD_GOOD));
                } else {
                    request.setAttribute(ERROR_ADD_GOOD_PARAMETER,
                            messageManager.getProperty(MESSAGE_ERROR_ADD_GOOD));
                }
                page = ConfigurationManager.getProperty(PATH_PAGE_ADD_GOOD);
                return page;
            }else{
                throw new LogicalException("Client data invalidate");
            }
        } catch (LogicalException e) {
            request.setAttribute(ERROR_EMPTY_FIELD_PARAMETER,
                    messageManager.getProperty(MESSAGE_EMPTY_FIELD));
            page = ConfigurationManager.getProperty(PATH_PAGE_ADD_GOOD);
            LOG.error(e);
        }
        return page;
    }
}
