package by.shymko.internetshop.servlet.command;

import by.shymko.internetshop.servlet.manager.ConfigurationManager;
import by.shymko.internetshop.user.User;

import javax.servlet.http.HttpServletRequest;

import static by.shymko.internetshop.constant.StringConstants.*;

/**
 * Created by Andrey on 09.05.2015
 */
public class BackCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        User current = (User) request.getSession().getAttribute(CURRENT_USER_PARAMETER);
        switch (current.getRole().name()) {
            case "ADMIN": {
                page = ConfigurationManager.getProperty(PATH_PAGE_ADMIN);
                break;
            }
            case "CLIENT": {
                page = ConfigurationManager.getProperty(PATH_PAGE_MAIN);
                break;
            }
            default:{
                page = ConfigurationManager.getProperty(PATH_PAGE_ERROR);
                break;
            }

        }
        return page;
    }
}
