package by.shymko.internetshop.servlet.command;

import by.shymko.internetshop.servlet.logic.DeleteLogic;
import by.shymko.internetshop.servlet.manager.ConfigurationManager;
import by.shymko.internetshop.servlet.manager.MessageManager;

import javax.servlet.http.HttpServletRequest;

import static by.shymko.internetshop.constant.StringConstants.PATH_PAGE_ADMIN;
import static by.shymko.internetshop.constant.StringConstants.SESSION_ATTRRIBUTE_LOCALE;

/**
 * Created by Andrey on 10.05.2015
 */
public class BlockClientCommand implements ActionCommand {
    private static final String REQUEST_PARAMETER_DELETED_ID = "idDelete";
    private static final String REQUEST_PARAMETER_DELETED_MESSAGE = "successfulclientdelete";
    private static final String REQUEST_PARAMETER_DELETED_FAIlD_MESSAGE = "faildclientdelete";
    private static final String MESSAGE_MANAGER_FAILD_DELETE = "message.faild.delete";
    private static final String MESSAGE_MANAGER_SUCCESSFUL_DELETE = "message.successful.delete";

    @Override
    public String execute(HttpServletRequest request) {
        MessageManager messageManager = new MessageManager((String) request.getSession().getAttribute(SESSION_ATTRRIBUTE_LOCALE));
        int idClient = Integer.parseInt(request.getParameter(REQUEST_PARAMETER_DELETED_ID));
        if(DeleteLogic.blockClient(idClient)){
            request.setAttribute(REQUEST_PARAMETER_DELETED_MESSAGE, messageManager.getProperty(MESSAGE_MANAGER_SUCCESSFUL_DELETE));
        }
        else{
            request.setAttribute(REQUEST_PARAMETER_DELETED_FAIlD_MESSAGE, messageManager.getProperty(MESSAGE_MANAGER_FAILD_DELETE));

        }
        return  ConfigurationManager.getProperty(PATH_PAGE_ADMIN);
    }
}
