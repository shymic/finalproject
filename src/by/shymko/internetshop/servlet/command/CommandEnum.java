package by.shymko.internetshop.servlet.command;

public enum CommandEnum {
    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },REGISTRATION {
        {
            this.command = new RegistrationCommand();
        }
    },SIGNUP {
        {
            this.command = new SignUPCommand();
        }
    },LOCALE {
        {
            this.command = new LocaleCommand();
        }
    },MAKEORDER{
        {
            this.command = new MakeOrderCommand();
        }
    },ADDGOOD{
        {
            this.command = new AddGoodCommand();
        }
    },BACK{
        {
            this.command = new BackCommand();
        }
    },ADD{
        {
            this.command = new AddCommand();
        }
    },BLOCKCLIENT{
        {
            this.command = new BlockClientCommand();
        }
    },DELETEGOOD{
        {
            this.command = new DeleteGoodCommand();
        }
    },SHOWORDER{
        {
            this.command = new ShowOrderCommand();
        }
    },REDACTORDER{
        {
            this.command = new RedactOrderCommand();
        }
    };
    ActionCommand command;
    public ActionCommand getCurrentCommand() {
        return command;
    }
}