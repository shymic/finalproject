package by.shymko.internetshop.servlet.command;

import by.shymko.internetshop.servlet.logic.DeleteLogic;
import by.shymko.internetshop.servlet.manager.ConfigurationManager;
import by.shymko.internetshop.servlet.manager.MessageManager;

import javax.servlet.http.HttpServletRequest;

import static by.shymko.internetshop.constant.StringConstants.PATH_PAGE_ADMIN;
import static by.shymko.internetshop.constant.StringConstants.SESSION_ATTRRIBUTE_LOCALE;


/**
 * Created by Andrey on 10.05.2015
 */
public class DeleteGoodCommand implements ActionCommand {
    private static final String REQUEST_PARAMETER_DELETED_ID = "idDelete";
    private static final String REQUEST_PARAMETER_DELETED_MESSAGE = "successfulgooddelete";
    private static final String REQUEST_PARAMETER_DELETED_FAILD_MESSAGE = "faildgooddelete";
    private static final String MESSAGE_MANAGER_SUCCESSFUL_DELETE = "message.good.successful.delete";
    private static final String MESSAGE_MANAGER_FAILD_DELETE = "message.good.faild.delete";

    @Override
    public String execute(HttpServletRequest request) {
        MessageManager messageManager = new MessageManager((String) request.getSession().getAttribute(SESSION_ATTRRIBUTE_LOCALE));
        int idGood = Integer.parseInt(request.getParameter(REQUEST_PARAMETER_DELETED_ID));
        if(DeleteLogic.deleteGoodById(idGood)){
            request.setAttribute(REQUEST_PARAMETER_DELETED_MESSAGE, messageManager.getProperty(MESSAGE_MANAGER_SUCCESSFUL_DELETE));
        }else{
            request.setAttribute(REQUEST_PARAMETER_DELETED_FAILD_MESSAGE, messageManager.getProperty(MESSAGE_MANAGER_FAILD_DELETE));
        }
        return  ConfigurationManager.getProperty(PATH_PAGE_ADMIN);
    }
}
