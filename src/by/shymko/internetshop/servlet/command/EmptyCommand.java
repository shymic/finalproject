package by.shymko.internetshop.servlet.command;

import by.shymko.internetshop.servlet.manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

import static by.shymko.internetshop.constant.StringConstants.PATH_PAGE_ERROR;

/**
 * Created by Andrey on 25.03.2015.
 */
public class EmptyCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
        return ConfigurationManager.getProperty(PATH_PAGE_ERROR);
    }
}