package by.shymko.internetshop.servlet.command;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Andrey on 21.04.2015.
 */
public class LocaleCommand implements ActionCommand {
    private static final String ATTRIBUTE_NAME_LOCALE = "locale";
    private static final String PARAMETER_NAME_LOCALE = "locale";
    private static final String PARAMETER_NAME_CURRENT_URL = "currentUrl";
    private static final String DEFAULT_LOCALE = "en";

    @Override
    public String execute(HttpServletRequest request) {
        String current = request.getParameter(PARAMETER_NAME_LOCALE);
        if ( current == null){
            current = DEFAULT_LOCALE;
        }
        request.getSession().setAttribute(ATTRIBUTE_NAME_LOCALE, current);
        return request.getParameter(PARAMETER_NAME_CURRENT_URL);
    }
}
