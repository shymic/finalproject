package by.shymko.internetshop.servlet.command;

import by.shymko.internetshop.exception.LogicalException;
import by.shymko.internetshop.servlet.logic.LoginLogic;
import by.shymko.internetshop.servlet.manager.ConfigurationManager;
import by.shymko.internetshop.servlet.manager.MessageManager;
import by.shymko.internetshop.user.User;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static by.shymko.internetshop.constant.StringConstants.*;

/**
 * Created by Andrey on 25.03.2015.
 */
public class LoginCommand implements ActionCommand {
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String ERROR_LOGIN_PARAMETER = "errorLoginPassMessage";
    private static final String MESSAGE_LOGIN_ERROR = "message.loginerror";

    private static final Logger LOG = Logger.getLogger(LoginCommand.class);


    @Override
    public String execute(HttpServletRequest request) {
        MessageManager messageManager = new MessageManager((String) request.getSession().getAttribute(SESSION_ATTRRIBUTE_LOCALE));

        String page = null;
        String login = request.getParameter(PARAM_NAME_LOGIN);
        String pass = DigestUtils.md5Hex(request.getParameter(PARAM_NAME_PASSWORD));

        User user = null;
        try {
            user =  LoginLogic.login(login, pass);
            if (user == null) {
                request.setAttribute(ERROR_LOGIN_PARAMETER,
                        messageManager.getProperty(MESSAGE_LOGIN_ERROR));
                page = ConfigurationManager.getProperty(PATH_PAGE_LOGIN);
            }else {
                request.getSession().setAttribute(CURRENT_USER_PARAMETER, user);
                if ("ADMIN".equalsIgnoreCase(user.getRole().name())) {
                    page = ConfigurationManager.getProperty(PATH_PAGE_ADMIN);
                }else{
                    page = ConfigurationManager.getProperty(PATH_PAGE_MAIN);
                }
            }
        } catch ( LogicalException e) {
            LOG.info(e);
        }
        return page;
    }
}