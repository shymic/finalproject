package by.shymko.internetshop.servlet.command;

import by.shymko.internetshop.servlet.manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Andrey on 25.03.2015.
 */
public class LogoutCommand implements ActionCommand {
    private static final String PATH_PAGE_INDEX = "path.page.index";

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty(PATH_PAGE_INDEX);
        request.getSession().invalidate();
        return page;
    }
}
