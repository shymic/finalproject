package by.shymko.internetshop.servlet.command;

import by.shymko.internetshop.list.GoodsList;
import by.shymko.internetshop.servlet.logic.OrderLogic;
import by.shymko.internetshop.servlet.manager.ConfigurationManager;
import by.shymko.internetshop.servlet.manager.MessageManager;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashMap;
import java.util.Map;

import static by.shymko.internetshop.constant.StringConstants.*;

/**
 * Created by Andrey on 07.05.2015.
 */
public class MakeOrderCommand implements ActionCommand {
    public static final String ATTRIBUTE_NAME = "chooseCount_";
    public static final String SUCCESSFUL_REGISTER_ORDER_PARAMETER = "successregisterorder";
    public static final String PRISELIST_PARAMETER = "priseList";
    public static final String MESSAGE_SUCCESSFUL_REGISTER_ORDER = "message.order.successfulregister";
    public static final String MESSAGE_FAILD_REGISTER_ORDER = "message.order.faildregister";
    public static final String FAILD_REGISTER_ORDER_PARAMETER = "faildregisterorder";



    @Override
    public String execute(HttpServletRequest request) {
        MessageManager messageManager = new MessageManager((String) request.getSession().getAttribute(SESSION_ATTRRIBUTE_LOCALE));
        GoodsList list = (GoodsList) request.getAttribute(PRISELIST_PARAMETER);
        Map<Integer, Integer> order = new LinkedHashMap<>();
        String page = null;
        for (int i = 0; i < list.size(); i++) {
            String count = request.getParameter(ATTRIBUTE_NAME + list.get(i).getId());
            if (count.length() != 0)
                order.put(list.get(i).getId(), Integer.parseInt(count));
        }
        if (order.size() > 0) {
            if (valid(order, list)) {
                request.setAttribute(SUCCESSFUL_REGISTER_ORDER_PARAMETER,
                        messageManager.getProperty(MESSAGE_SUCCESSFUL_REGISTER_ORDER));
                OrderLogic.makeOrder(request, order);

            } else {
                request.setAttribute(FAILD_REGISTER_ORDER_PARAMETER,
                        messageManager.getProperty(MESSAGE_FAILD_REGISTER_ORDER));
            }
            page = ConfigurationManager.getProperty(PATH_PAGE_SHOW);
        } else {
            page = ConfigurationManager.getProperty(PATH_PAGE_MAIN);
        }
        return page;
    }

    private static boolean valid(Map<Integer, Integer> order, GoodsList list) {
        boolean flag = true;
        for (int i = 0; i < list.size(); ++i) {
            if (order.containsKey(list.get(i).getId())) {
                if (order.get(list.get(i).getId()) > list.get(i).getQuantity()) {
                    flag = false;
                }
            }
        }
        return flag;
    }
}
