package by.shymko.internetshop.servlet.command;

import by.shymko.internetshop.servlet.logic.DeleteLogic;
import by.shymko.internetshop.servlet.manager.ConfigurationManager;
import by.shymko.internetshop.servlet.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static by.shymko.internetshop.constant.StringConstants.PATH_PAGE_SHOW;
import static by.shymko.internetshop.constant.StringConstants.SESSION_ATTRRIBUTE_LOCALE;

/**
 * Created by Andrey on 10.05.2015
 */
public class RedactOrderCommand implements ActionCommand {
    private static final Logger LOG = Logger.getLogger(RedactOrderCommand.class);
    private static final String REQUEST_PARAMETER_DELETED_ID = "idGood";
    private static final String REQUEST_PARAMETER_DELETED_MESSAGE = "successfulordercancel";
    private static final String REQUEST_PARAMETER_DELETED_FAIlD_MESSAGE = "faildordercancel";
    private static final String MESSAGE_MANAGER_SUCCESSFUL_DELETE = "message.successful.cancel.order";
    private static final String MESSAGE_MANAGER_FAILD_DELETE = "message.faild.cancel.order";

    @Override
    public String execute(HttpServletRequest request) {
        MessageManager messageManager = new MessageManager((String) request.getSession().getAttribute(SESSION_ATTRRIBUTE_LOCALE));
        int idGood = Integer.parseInt(request.getParameter(REQUEST_PARAMETER_DELETED_ID));
        if(DeleteLogic.deleteGoodByIdFromOrder(idGood)){
            request.setAttribute(REQUEST_PARAMETER_DELETED_MESSAGE, messageManager.getProperty(MESSAGE_MANAGER_SUCCESSFUL_DELETE));
        }
        else{
            request.setAttribute(REQUEST_PARAMETER_DELETED_FAIlD_MESSAGE, messageManager.getProperty(MESSAGE_MANAGER_FAILD_DELETE));

        }
        return  ConfigurationManager.getProperty(PATH_PAGE_SHOW);
    }
}
