package by.shymko.internetshop.servlet.command;

import by.shymko.internetshop.servlet.manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Andrey on 12.04.2015.
 */
public class RegistrationCommand implements ActionCommand {
    private static final String PATH_PAGE_REGISTRATION = "path.page.registration";

    @Override
    public String execute(HttpServletRequest request) {
        return ConfigurationManager.getProperty(PATH_PAGE_REGISTRATION);
    }
}
