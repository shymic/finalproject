package by.shymko.internetshop.servlet.command;

import by.shymko.internetshop.order.ShowOrderRecord;
import by.shymko.internetshop.servlet.logic.ShowOrderLogic;
import by.shymko.internetshop.servlet.manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

import static by.shymko.internetshop.constant.StringConstants.PATH_PAGE_SHOW;

/**
 * Created by Andrey on 10.05.2015
 */
public class ShowOrderCommand implements ActionCommand {
    public static final String ATTRIBUTE_NAME = "idClient";
    public static final String PARAMETER_ORDER = "order";


    @Override
    public String execute(HttpServletRequest request) {

        int idClient = Integer.parseInt(request.getParameter(ATTRIBUTE_NAME));
        ArrayList<ShowOrderRecord> order = ShowOrderLogic.selectOrder(idClient);
        request.setAttribute(PARAMETER_ORDER, order);

        return ConfigurationManager.getProperty(PATH_PAGE_SHOW);

    }
}
