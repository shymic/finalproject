package by.shymko.internetshop.servlet.command;

import by.shymko.internetshop.exception.LogicalException;
import by.shymko.internetshop.generator.IdClientGenerator;
import by.shymko.internetshop.servlet.logic.RegistrationLogic;
import by.shymko.internetshop.servlet.manager.ConfigurationManager;
import by.shymko.internetshop.servlet.manager.MessageManager;
import by.shymko.internetshop.user.Client;
import by.shymko.internetshop.validator.Validator;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static by.shymko.internetshop.constant.StringConstants.PATH_PAGE_REGISTRATION;
import static by.shymko.internetshop.constant.StringConstants.SESSION_ATTRRIBUTE_LOCALE;

/**
 * Created by Andrey on 12.04.2015.
 */
public class SignUPCommand implements ActionCommand {
    private static final String REQUEST_PARAMETER_LOGIN = "login";
    private static final String REQUEST_PARAMETER_PASSWORD = "password";
    private static final String REQUEST_PARAMETER_CONFIRM = "confirm";
    private static final String REQUEST_PARAMETER_BUDGET = "budget";
    private static final String REQUEST_PARAMETER_CONTACT = "contact";
    private static final String ERROR_CONFIRM_PASS_PARAMETER = "errorConfirmPassMessage";
    private static final String ERROR_USER_EXIST_PARAMETER = "errorUserExistMessage";
    private static final String COMPLETE_USER_REGISTRATION_PARAMETER = "completeRegistration";
    private static final String ERROR_EMPTY_FIELD_PARAMETER = "emptyField";
    private static final String PATH_PAGE_LOGIN = "path.page.login";
    private static final String MESSAGE_CONFIRM_ERROR = "message.confirmerror";
    private static final String MESSAGE_USER_EXIST = "message.userexist";
    private static final String MESSAGE_COMPLETE_REGISTRATION = "message.completeregistration";
    private static final String MESSAGE_EMPTY_FIELD = "message.emptyfield";

    private static final Logger LOG = Logger.getLogger(SignUPCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        MessageManager messageManager = new MessageManager((String) request.getSession().getAttribute(SESSION_ATTRRIBUTE_LOCALE));
        Client client = new Client();
        String page = null;
        try {
            if (request.getParameter(REQUEST_PARAMETER_PASSWORD).compareTo(request.getParameter(REQUEST_PARAMETER_CONFIRM) )!= 0) {
                request.setAttribute(ERROR_CONFIRM_PASS_PARAMETER,
                        messageManager.getProperty(MESSAGE_CONFIRM_ERROR));
                page = ConfigurationManager.getProperty(PATH_PAGE_REGISTRATION);
                return page;
            }
            client.setId(IdClientGenerator.generate());
            client.setLogin(request.getParameter(REQUEST_PARAMETER_LOGIN));
            client.setPassword(DigestUtils.md5Hex(request.getParameter(REQUEST_PARAMETER_PASSWORD)));
            client.setContact(request.getParameter(REQUEST_PARAMETER_CONTACT));
            client.setBudget(Integer.parseInt(request.getParameter(REQUEST_PARAMETER_BUDGET)));
            client.setActivity(true);
            if(Validator.validateClient(client)) {
                if (RegistrationLogic.registration(client)) {
                    request.setAttribute(COMPLETE_USER_REGISTRATION_PARAMETER,
                            messageManager.getProperty(MESSAGE_COMPLETE_REGISTRATION));
                    page = ConfigurationManager.getProperty(PATH_PAGE_LOGIN);
                } else {
                    request.setAttribute(ERROR_USER_EXIST_PARAMETER,
                            messageManager.getProperty(MESSAGE_USER_EXIST));
                    page = ConfigurationManager.getProperty(PATH_PAGE_REGISTRATION);
                    return page;
                }
            }else{
                throw new LogicalException("Client data invalidate");
            }
        } catch (LogicalException e) {
            request.setAttribute(ERROR_EMPTY_FIELD_PARAMETER,
                    messageManager.getProperty(MESSAGE_EMPTY_FIELD));
            page = ConfigurationManager.getProperty(PATH_PAGE_REGISTRATION);
            LOG.error(e);
        }
        return page;
    }
}
