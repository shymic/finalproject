package by.shymko.internetshop.servlet.controller;

import by.shymko.internetshop.servlet.command.ActionCommand;
import by.shymko.internetshop.servlet.command.CommandEnum;
import by.shymko.internetshop.servlet.command.EmptyCommand;
import by.shymko.internetshop.servlet.manager.MessageManager;

import javax.servlet.http.HttpServletRequest;

import static by.shymko.internetshop.constant.StringConstants.SESSION_ATTRRIBUTE_LOCALE;

/**
 * Created by Andrey on 25.03.2015.
 */
public class ActionFactory {
    private static final String WRONG_ACTION_PARAMETER = "wrongAction";
    private static final String MESSAGE_WRONG_ACTION = "message.wrongaction";
    private static final String COMMAND_ACTION_PARAMETER = "command";

    /**
     * @param request
     * @return
     */
    public ActionCommand defineCommand(HttpServletRequest request) {
        MessageManager messageManager = new MessageManager((String) request.getSession().getAttribute(SESSION_ATTRRIBUTE_LOCALE));
        ActionCommand current = new EmptyCommand();

        String action = request.getParameter(COMMAND_ACTION_PARAMETER);
        if (action == null || action.isEmpty()) {
            return current;
        }
        try {
            CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
            current = currentEnum.getCurrentCommand();
        } catch (IllegalArgumentException e) {
            request.setAttribute(WRONG_ACTION_PARAMETER, action
                    + messageManager.getProperty(MESSAGE_WRONG_ACTION));
        }
        return current;
    }
}