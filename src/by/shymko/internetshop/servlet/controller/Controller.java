package by.shymko.internetshop.servlet.controller;

import by.shymko.internetshop.exception.LogicalException;
import by.shymko.internetshop.pool.PoolConnection;
import by.shymko.internetshop.servlet.command.ActionCommand;
import by.shymko.internetshop.servlet.manager.ConfigurationManager;
import by.shymko.internetshop.servlet.manager.MessageManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.shymko.internetshop.constant.StringConstants.*;

/**
 * Created by Andrey on 25.03.2015.
 */
@WebServlet("/controller")
public class Controller extends HttpServlet {
    private static final String LOGGER_XML_PATH = "/resources/log4j.xml";


    private static final Logger LOG = Logger.getLogger(Controller.class);

    @Override
    public void init() throws ServletException {
        new DOMConfigurator().doConfigure(getServletContext().getRealPath(LOGGER_XML_PATH),
                LogManager.getLoggerRepository());
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void processRequest(HttpServletRequest request,
                                HttpServletResponse response)
            throws ServletException, IOException {
        String page = null;
        ActionFactory action = new ActionFactory();



        ActionCommand command = action.defineCommand(request);
        MessageManager messageManager = new MessageManager((String) request.getSession().getAttribute(SESSION_ATTRRIBUTE_LOCALE));

        page = command.execute(request);

        if (page != null) {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } else {
            page = ConfigurationManager.getProperty(PATH_PAGE_ERROR);
            request.getSession().setAttribute(ERROR_NULL_PAGE,
                    messageManager.getProperty(MESSAGE_NULLPAGE));
            response.sendRedirect(request.getContextPath() + page);
        }
    }

    /**
     *
     */
    @Override
    public void destroy() {
        try {
            PoolConnection.releasePool();
        } catch (LogicalException e) {
            LOG.info(e);
        }
    }
}