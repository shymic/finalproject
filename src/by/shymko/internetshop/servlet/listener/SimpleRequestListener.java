package by.shymko.internetshop.servlet.listener;

import by.shymko.internetshop.list.ClientList;
import by.shymko.internetshop.list.GoodsList;
import by.shymko.internetshop.servlet.logic.ClientListLogic;
import by.shymko.internetshop.servlet.logic.MenuLogic;
import by.shymko.internetshop.user.Administrator;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

import static by.shymko.internetshop.constant.StringConstants.ADMINLIST_PARAMETER;
import static by.shymko.internetshop.constant.StringConstants.CLIENTLIST_PARAMETER;
import static by.shymko.internetshop.constant.StringConstants.PRISELIST_PARAMETER;

/**
 * Created by Andrey on 11.05.2015
 */
@WebListener
public class SimpleRequestListener implements ServletRequestListener {

    /**
     * @param ev
     */
    @Override
    public void requestInitialized(ServletRequestEvent ev) {
        HttpServletRequest request = (HttpServletRequest) ev.getServletRequest();
        GoodsList priseList = MenuLogic.takeGoodList();
        request.setAttribute(PRISELIST_PARAMETER, priseList);

        ClientList clientList  = ClientListLogic.takeClientList();
        request.setAttribute(CLIENTLIST_PARAMETER, clientList);

        ArrayList<Administrator> adminList = ClientListLogic.takeAdminList();
        request.setAttribute(ADMINLIST_PARAMETER, adminList);
    }
    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {

    }

}