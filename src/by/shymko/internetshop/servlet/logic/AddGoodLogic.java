package by.shymko.internetshop.servlet.logic;

import by.shymko.internetshop.dao.AdminDAO;
import by.shymko.internetshop.good.Good;
import by.shymko.internetshop.pool.PoolConnection;

import java.sql.Connection;

/**
 * Created by Andrey on 09.05.2015
 */
public class AddGoodLogic {

    /**
     * @param newGood
     * @return
     */
    public static boolean add(Good newGood) {
        Connection connection = PoolConnection.getPool().getConnection();
        AdminDAO dao = new AdminDAO(connection);
        boolean flag = dao.addGood(newGood);
        PoolConnection.returnConnection(connection);
        return flag;
    }
}
