package by.shymko.internetshop.servlet.logic;

import by.shymko.internetshop.dao.AdminDAO;
import by.shymko.internetshop.dao.UserDAO;
import by.shymko.internetshop.list.ClientList;
import by.shymko.internetshop.pool.PoolConnection;
import by.shymko.internetshop.user.Administrator;

import java.sql.Connection;
import java.util.ArrayList;

/**
 * Created by Andrey on 10.05.2015
 */
public class ClientListLogic {

    /**
     * @return
     */
    public static ClientList takeClientList() {
        ClientList clientList = null;
        Connection connection = PoolConnection.getPool().getConnection();
        UserDAO dao = new UserDAO(connection);
        clientList = dao.selectClientList();
        PoolConnection.returnConnection(connection);
        return clientList;
    }

    /**
     * @return
     */
        public static ArrayList<Administrator> takeAdminList() {
        ArrayList<Administrator> adminList = null;
        Connection connection = PoolConnection.getPool().getConnection();
        AdminDAO dao = new AdminDAO(connection);
        adminList = dao.selectAdminList();
        PoolConnection.returnConnection(connection);
        return adminList;
    }



}
