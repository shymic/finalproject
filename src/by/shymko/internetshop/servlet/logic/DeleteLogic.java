package by.shymko.internetshop.servlet.logic;

import by.shymko.internetshop.dao.AdminDAO;
import by.shymko.internetshop.pool.PoolConnection;

import java.sql.Connection;

/**
 * Created by Andrey on 10.05.2015
 */
public class DeleteLogic {
    /**
     * @param id
     * @return
     */
    public static boolean blockClient(int id){
        boolean flag = false;
        Connection connection = PoolConnection.getPool().getConnection();
        AdminDAO dao = new AdminDAO(connection);
        flag = dao.deleteClientById(id);
        PoolConnection.returnConnection(connection);
        return flag;
    }

    /**
     * @param id
     * @return
     */
    public static boolean deleteGoodById(int id){
        boolean flag = false;
        Connection connection = PoolConnection.getPool().getConnection();
        AdminDAO dao = new AdminDAO(connection);
        flag = dao.deleteGoodById(id);
        PoolConnection.returnConnection(connection);
        return flag;
    }

    /**
     * @param id
     * @return
     */
    public static boolean deleteGoodByIdFromOrder(int id){
        boolean flag = false;
        Connection connection = PoolConnection.getPool().getConnection();
        AdminDAO dao = new AdminDAO(connection);
        flag = dao.deleteGoodByIdFromOrder(id);
        PoolConnection.returnConnection(connection);
        return flag;
    }

}
