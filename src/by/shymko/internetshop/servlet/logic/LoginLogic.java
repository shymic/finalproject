package by.shymko.internetshop.servlet.logic;

import by.shymko.internetshop.dao.UserDAO;
import by.shymko.internetshop.exception.LogicalException;
import by.shymko.internetshop.pool.PoolConnection;
import by.shymko.internetshop.user.User;

import java.sql.Connection;

/**
 * Created by Andrey on 25.03.2015.
 */
public class LoginLogic {

    /**
     * @param login
     * @param password
     * @return
     * @throws LogicalException
     */
    public static User login( String login, String password) throws  LogicalException {
        Connection connection = PoolConnection.getPool().getConnection();
        UserDAO dao = new UserDAO(connection);
        User user = dao.selectUserByLoginAndPassword(login, password);
        PoolConnection.returnConnection(connection);
        return user;
    }

}