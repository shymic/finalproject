package by.shymko.internetshop.servlet.logic;

import by.shymko.internetshop.dao.UserDAO;
import by.shymko.internetshop.exception.LogicalException;
import by.shymko.internetshop.list.GoodsList;
import by.shymko.internetshop.pool.PoolConnection;
import org.apache.log4j.Logger;

import java.sql.Connection;

/**
 * Created by Andrey on 26.04.2015.
 */
public class MenuLogic {
    private static final Logger LOG = Logger.getLogger(MenuLogic.class);

    /**
     * @return
     */
    public static GoodsList takeGoodList() {
        GoodsList goodsList = null;
        try {
            Connection connection = PoolConnection.getPool().getConnection();
            UserDAO dao = new UserDAO(connection);
            goodsList = dao.selectGoodList();
            PoolConnection.returnConnection(connection);
        } catch (LogicalException e) {
            LOG.info(e);
        }
        return goodsList;
    }
}
