package by.shymko.internetshop.servlet.logic;

import by.shymko.internetshop.dao.UserDAO;
import by.shymko.internetshop.pool.PoolConnection;
import by.shymko.internetshop.user.User;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.util.Map;

/**
 * Created by Andrey on 08.05.2015.
 */
public class OrderLogic {
    private static final String CURRENT_USER_PARAMETER = "currentUser";

    /**
     * @param request
     * @param order
     * @return
     */
    public static boolean makeOrder(HttpServletRequest request, Map<Integer, Integer> order) {
        Connection connection = null;
        boolean flag = false;
        try {
            connection = PoolConnection.getPool().getConnection();
            UserDAO dao = new UserDAO(connection);
            User user = (User) request.getSession().getAttribute(CURRENT_USER_PARAMETER);
            flag = dao.registerOrder(order, user);
        } finally {
            PoolConnection.returnConnection(connection);
        }
        return flag;
    }
}
