package by.shymko.internetshop.servlet.logic;

import by.shymko.internetshop.dao.UserDAO;
import by.shymko.internetshop.exception.LogicalException;
import by.shymko.internetshop.pool.PoolConnection;
import by.shymko.internetshop.user.Client;
import org.apache.log4j.Logger;

import java.sql.Connection;

/**
 * Created by Andrey on 12.04.2015.
 */
public class RegistrationLogic {
    private static final Logger LOG = Logger.getLogger(RegistrationLogic.class);

    /**
     * @param client
     * @return
     */
    public static boolean registration(Client client){
        Connection connection = PoolConnection.getPool().getConnection();
        UserDAO dao = new UserDAO(connection);
        boolean flag = false;
        try {
            if(dao.selectUserByLogin(client.getLogin())== null) {
                flag = dao.addClient(client);
            }else{
                LOG.info("same login user exist");
            }
        } catch (LogicalException e) {
            LOG.info(e);
        }finally {
            PoolConnection.returnConnection(connection);
        }
        return flag;
    }
}
