package by.shymko.internetshop.servlet.logic;

import by.shymko.internetshop.dao.UserDAO;
import by.shymko.internetshop.order.ShowOrderRecord;
import by.shymko.internetshop.pool.PoolConnection;

import java.sql.Connection;
import java.util.ArrayList;

/**
 * Created by Andrey on 10.05.2015
 */
public class ShowOrderLogic {
    /**
     * @param idClient
     * @return
     */
    public static ArrayList<ShowOrderRecord> selectOrder(int idClient){
        Connection connection = PoolConnection.getPool().getConnection();
        UserDAO dao = new UserDAO(connection);
        ArrayList<ShowOrderRecord> order =  dao.selectOrderbByClientId(idClient);
        PoolConnection.returnConnection(connection);
        return order;

    }


}
