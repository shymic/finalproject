package by.shymko.internetshop.servlet.manager;

import java.util.ResourceBundle;

/**
 * Created by Andrey on 25.03.2015.
 */
public class ConfigurationManager {
    private static final String CONFIG_RESOURCE_PATH = "resources/config";

    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle(CONFIG_RESOURCE_PATH);
    private ConfigurationManager() { }
    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}