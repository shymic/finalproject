package by.shymko.internetshop.servlet.manager;

import org.apache.log4j.Logger;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Created by Andrey on 25.03.2015.
 */
public class MessageManager {
    private static final String MESSAGE_RESOURCE_PATH = "resources/message/messages";
    private static final String DEFAULT_LOCALE = "en";
    private static final Logger LOG = Logger.getLogger(MessageManager.class);

    private ResourceBundle resourceBundle;
    public MessageManager(String locale) {
        if (locale == null)
        {
            locale = DEFAULT_LOCALE;
        }
        resourceBundle = ResourceBundle.getBundle(MESSAGE_RESOURCE_PATH, Locale.forLanguageTag(locale));
    }
    public String getProperty(String key) {
        try {
            return resourceBundle.getString(key);
        } catch (MissingResourceException e) {
            LOG.info("could not find value in message properties by key:" + key);
        }
        return null;
    }
}
