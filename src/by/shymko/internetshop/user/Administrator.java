package by.shymko.internetshop.user;


import by.shymko.internetshop.exception.LogicalException;
import by.shymko.internetshop.good.Good;
import by.shymko.internetshop.list.ClientList;
import by.shymko.internetshop.list.GoodsList;
import org.apache.log4j.Logger;


/**
 * Created by Andrey on 17.02.2015.
 */
public class Administrator extends User {
    private static final Logger LOG = Logger.getLogger(Administrator.class);
    private static final Role ROLE = Role.ADMIN;
    private GoodsList priseList;
    private ClientList clientList;

    public Administrator() {
    }

    public Administrator(int id, String name, String password, String contact) {
        super(id, name, password, contact);
    }

    public Administrator(int id, String name,  String password, String contact,
                         ClientList clientList, GoodsList priseList) {
        super(id, name, password, contact);
        this.priseList = priseList;
        this.clientList = clientList;
    }

    public GoodsList getPriseList() {
        return priseList;
    }

    public void setPriseList(GoodsList priseList) throws LogicalException {
        if (priseList == null) {
            throw new LogicalException("prise list is null");
        }
        this.priseList = priseList;
    }

    public Role getRole() {
        return ROLE;
    }

    public ClientList getClientList() {
        return clientList;
    }


    public void setClientList(ClientList clientList) throws LogicalException {
        if (clientList == null) {
            throw new LogicalException("client list is null");
        }
        this.clientList = clientList;
    }

    public boolean addClient(Client client) {
        return clientList.add(client);
    }

    public boolean removeGood(int id) {
        return getPriseList().removeById(id);
    }

    public boolean addGood(Good good) {
        return priseList.add(good);
    }

    public boolean blockClient(int id) {
        if (clientList.findById(id) != null) {
            clientList.findById(id).setActivity(false);
            LOG.info(" client was blocked");
            return true;
        } else {
            LOG.info("id not found");
            return false;
        }
    }

    public boolean unblockClient(int id) {
        if (clientList.findById(id) != null) {
            clientList.findById(id).setActivity(true);
            LOG.info(" client was unblocked");
            return true;
        } else {
            LOG.info("id not found");
            return false;
        }
    }

    @Override
    public String toString() {
        return "Administrator{" + "id=" + super.getId() + '\n' +
                ", login='" + super.getLogin() + '\n' +
                "contact='" + super.getContact() + '\n' +
                ", priseList=" + priseList + '\n' +
                ", clientList=" + clientList + '\n' +
                '}';
    }


}
