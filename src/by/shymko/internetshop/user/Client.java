package by.shymko.internetshop.user;

import by.shymko.internetshop.exception.LogicalException;
import by.shymko.internetshop.list.GoodsList;


/**
 * Created by Andrey on 17.02.2015.
 */
public class Client extends User {
    //private static final Logger LOG = Logger.getLogger(Client.class);
    private static final Role ROLE = Role.CLIENT;
    private GoodsList order;
    private int budget;
    private boolean activity;

    public Client() {
    }

    public Client(int id, String name, String password, String contact) {
        super(id, name, password, contact);
    }

    public Client(int id, String name, String password, String contact, int budget, boolean activity) {
        super(id, name, password, contact);
        this.budget = budget;
        this.activity = activity;
    }

    public Client(int id, String name, String password, String contact, GoodsList order, int budget, boolean activity) {
        super(id, name, password, contact);
        this.order = order;
        this.budget = budget;
        this.activity = activity;
    }


    public GoodsList getOrder() {
        return order;
    }

    public void setOrder(GoodsList order) throws LogicalException {
        if (order == null) {
            throw new LogicalException("order list is null");
        }
        this.order = order;
    }

    public Role getRole() {
        return ROLE;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) throws LogicalException {
        if (budget < 0) {
            throw new LogicalException("invalid budget");
        }
        this.budget = budget;
    }

    public boolean isActivity() {
        return activity;
    }

    public void setActivity(boolean activity) {
        this.activity = activity;
    }

    @Override
    public String toString() {
        return "Client{" + "id=" + super.getId() +
                ", login='" + super.getLogin() +
                ", contact='" + super.getContact() +
                "order=" + order +
                ", budget=" + budget +
                ", activity=" + activity +
                '}';
    }


}
