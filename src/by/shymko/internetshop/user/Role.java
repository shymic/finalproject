package by.shymko.internetshop.user;

/**
 * Created by Andrey on 13.04.2015.
 */
public enum Role {

    ADMIN{
        {
            this.name = "ADMIN";
        }
    }, CLIENT{
        {
            this.name="CLIENT";
        }
    };
    public String name;

    public String getName() {
        return name;
    }
}
