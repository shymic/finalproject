package by.shymko.internetshop.user;

import by.shymko.internetshop.exception.LogicalException;

/**
 * Created by Andrey on 17.02.2015.
 */
public abstract class User {
    private int id;
    private String login;
    private String password;
    private String contact;

    protected User() {
    }

    public User(int id, String login, String password, String contact) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.contact = contact;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) throws LogicalException {
        if (contact.length() == 0) {
            throw new LogicalException("invalid contact");
        }
        this.contact = contact;
    }

    public String getPassword() {
        return password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) throws LogicalException {
        if (id < 0) {
            throw new LogicalException("invalid id");
        }
        this.id = id;
    }

       public String getLogin() {
        return login;
    }

    public void setLogin(String login) throws LogicalException {
        if (login.length() == 0) {
            throw new LogicalException();
        }
        this.login = login;
    }

    public void setPassword(String password) throws LogicalException {
        if (password.length() == 0) {
            throw new LogicalException();
        }
        this.password = password;
    }

    public Role getRole(){
        return null;
    }

    public int getBudget(){
        return 0;
    }

}
