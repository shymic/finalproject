package by.shymko.internetshop.validator;

import by.shymko.internetshop.good.Good;
import by.shymko.internetshop.user.Client;

/**
 * Created by Andrey on 26.04.2015
 */
public class Validator {
    public static final String USER_LOGIN_REGEX ="\\w{4,16}";
    public static final String USER_PASSWORD_REGEX ="\\w{6,16}";
    public static final String USER_CONTACT_REGEX =".{1,255}";
    public static final String GOOD_INFO_REGEX =".{1,255}";
    public static final String GOOD_NAME_REGEX ="\\w{4,16}";


    /**
     * @param client
     * @return
     */
    public static boolean validateClient(Client client){
        return client.getLogin().matches(USER_LOGIN_REGEX) && client.getContact().matches(USER_CONTACT_REGEX);
    }

    /**
     * @param good
     * @return
     */
    public static boolean validateGood(Good good){
        if (good.getQuantity()<=0){
            return false;
        }
        if (good.getPrise()<=0){
            return false;
        }
        if (good.getSale()<=0){
            return false;
        }
        return good.getName().matches(GOOD_NAME_REGEX) && good.getInfo().matches(GOOD_INFO_REGEX);
    }
}
