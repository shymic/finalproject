<%--
  Created by IntelliJ IDEA.
  User: Andrey
  Date: 09.05.2015
  Time: 16:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jspf/init.jspf" %>

<html>
<head>
    <title> <fmt:message key="addgood.title"/></title>
</head>
<body>

<h1><fmt:message key="addgood.head"/></h1>
<form class="addgood" name="addgood" method="POST" action="controller">
    <input type="hidden" name="command" value="addgood" />

    <input type="text" name="name" placeholder="<fmt:message key="addgood.field.name"/>" value="" class="required" pattern="[\w]{4,16}"/>
    <p class="hints">*<fmt:message key="addgood.name.hint"/></p>
    <br/>
    <input type="text" name="prise" placeholder="<fmt:message key="addgood.field.prise"/>" value="" class="required" pattern="[\d]*"/>
    <p class="hints">*</p>
    <br/>
    <input type="text" name="sale" placeholder="<fmt:message key="addgood.field.sale"/>" value="" class="required" pattern="[\d]*"/>
    <p class="hints">*</p>
    <br/>
    <input type="text" name="quantity" placeholder="<fmt:message key="addgood.field.quantity"/>" value="" class="required" pattern="[\d]*"/>
    <p class="hints">*</p>
    <br/>
    <input type="text" name="info" placeholder="<fmt:message key="addgood.field.info"/>" value="" class="required" maxlength="255"/>
    <p class="hints">*<fmt:message key="addgood.info.hint"/></p>

    <br/>
    <p class="errors">${errorAddGoodMessage}</p>

    <p class="errors">${wrongAction}</p>

    <p class="errors">${nullPage}</p>

    <p class="errors">${emptyField}</p>
    <p class="success">${completeAddGoodMessage}</p>
    <br/>
    <input type="submit" value="<fmt:message key="addgood.button.submit"/>"/>
</form><hr/>
<%@ include file="/WEB-INF/jspf/back.jspf" %>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>
