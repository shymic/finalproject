<%--
  Created by IntelliJ IDEA.
  User: Andrey
  Date: 09.05.2015
  Time: 17:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jspf/init.jspf" %>

<html>
<head>
    <title><fmt:message key="admin.title"/></title>
</head>
<body>
<h3><fmt:message key="admin.head"/></h3>
<hr/>
<p class="success" id="gooddelete">${successfulgooddelete}</p>
<p class="errors" id="gooddelete">${faildgooddelete}</p>
<%@ include file="/WEB-INF/jspf/adminPriseList.jspf" %>

<a href="controller?command=add"><fmt:message key="command.add"/></a>

<hr/>
<h3><fmt:message key="content.users"/></h3>
<%@ include file="/WEB-INF/jspf/clientList.jspf" %>
<p class="success">${successfulclientdelete}</p>
<p class="errors">${faildclientdelete}</p>
<hr/>
<h3><fmt:message key="content.admins"/></h3>
<%@ include file="/WEB-INF/jspf/adminList.jspf" %>
<hr/>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>
