<%--
  Created by IntelliJ IDEA.
  User: Andrey
  Date: 25.03.2015
  Time: 10:56
  To change this template use File | Settings | File Templates.
  <%@ include file="/WEB-INF/jspf/init.jspf" %>
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/init.jspf" %>



<html>
<head>
    <title>
        <fmt:message key="login.title"/>
    </title>
</head>
<body>

<form name="loginForm" method="POST" action="controller">
    <input type="hidden" name="command" value="login" />

    <input type="text" name="login" placeholder="<fmt:message key="login.login"/>" value=""/>
    <br/>
    <input type="password" name="password" placeholder="<fmt:message key="login.password"/>" value=""/>
    <br/>
    <p class="errors">${errorLoginPassMessage}</p>
    <p class ="errors">${wrongAction}</p>
    <p class="errors">${nullPage}</p>
    <br/>
    <a href="controller?command=registration"><fmt:message key="login.href.registration"/></a>
    <br/>
    <p class="success">${completeRegistration}</p>
    <input  class="enviar" type="submit" value="<fmt:message key="login.button.submit"/>"/>

</form>

<%@ include file="/WEB-INF/jspf/static.jspf" %>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body></html>
